=================================================
Accenta RL Calibration XPs for ENS Challenge Data
=================================================

Copyright (c) 2022 Jérémie Decock

* Web site: https://gitlab.com/ens-data-challenge/rl-calibration


Description
===========

Accenta RL calibration experiments for ENS Challenge Data

Note:

    This project is still in beta stage, so the API is not finalized yet.


Dependencies
============

C.f. requirements.txt

.. _install:

Installation
============

Install Redis Server (Debian)
-----------------------------

::

    apt install redis


Posix (Linux, MacOSX, WSL, ...)
-------------------------------

From the RL Agent source code::

    conda deactivate         # Only if you use Anaconda...
    python3 -m venv env
    source env/bin/activate
    python3 -m pip install --upgrade pip
    python3 -m pip install -r requirements.txt
    python3 setup.py develop


Windows
-------

From the RL Agent source code::

    conda deactivate         # Only if you use Anaconda...
    python3 -m venv env
    env\Scripts\activate.bat
    python3 -m pip install --upgrade pip
    python3 -m pip install -r requirements.txt
    python3 setup.py develop


Example usage with Celery
=========================

C.f. https://docs.celeryproject.org/en/stable/getting-started/first-steps-with-celery.html#installing-celery

Run the Celery worker server::

    source env/bin/activate
    celery -A rlaxp worker -l INFO


Run the Celery client::

    source env/bin/activate
    ./client.py


View training on TensorBoard::

    source env/bin/activate
    tensorboard --logdir=tfboard


Documentation
=============

* Online documentation: https://ens-data-challenge.gitlab.io/accenta-rl-agent-example
* API documentation: https://ens-data-challenge.gitlab.io/accenta-rl-agent-example/api.html


Example usage
=============

C.f. https://ens-data-challenge.gitlab.io/accenta-rl-agent-example/gallery/


Bug reports
===========

To search for bugs or report them, please use the Accenta's RL Agent Example Bug Tracker at:

    https://gitlab.com/ens-data-challenge/accenta-rl-agent-example/issues
