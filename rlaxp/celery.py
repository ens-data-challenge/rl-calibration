#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from celery import Celery

PYTHON_PKG_NAME = 'rlaxp'

app = Celery(PYTHON_PKG_NAME,
             broker='redis://localhost',
             backend='redis://localhost',
             include=[PYTHON_PKG_NAME + '.tasks'])

# Optional configuration, see the application user guide.
app.conf.update(
    result_expires=3600,
)

if __name__ == '__main__':
    app.start()
