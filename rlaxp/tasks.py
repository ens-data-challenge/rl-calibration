from .celery import app

from rlenv.envs.wall.core import AccentaEnv

import torch as th
from stable_baselines3 import PPO

import json
import datetime
import time
import os
import socket


MODELS_DIR = "models"
TENSORBOARD_DIR = "tfboard"

os.makedirs(MODELS_DIR, exist_ok=True)
os.makedirs(TENSORBOARD_DIR, exist_ok=True)

@app.task
def train_agent(ppo_pi_net_arch=(32, 32),
                ppo_vf_net_arch=(32, 32),
                ppo_activation_fn=th.nn.ReLU,
                ppo_policy_class="MlpPolicy",
                train_verbose_level=1,
                train_timesteps=10000,
                device='cpu'):

    d = datetime.datetime.now()
    d_str = f"{d.year}-{d.month}-{d.day}_{d.hour}-{d.minute}-{d.second}_{d.microsecond}"
    pid = os.getpid()


    ###############################################################################
    # Make the environment

    env = AccentaEnv()


    ###############################################################################
    # Make the agent

    # Custom actor (pi) and value function (vf) networks
    # of two layers of size 32 each with Relu activation function
    # https://stable-baselines3.readthedocs.io/en/master/guide/custom_policy.html#custom-network-architecture
    policy_kwargs = dict(activation_fn=ppo_activation_fn,
                        net_arch=[dict(pi=list(ppo_pi_net_arch), vf=list(ppo_vf_net_arch))])

    model = PPO(ppo_policy_class, env, policy_kwargs=policy_kwargs, verbose=train_verbose_level, tensorboard_log=TENSORBOARD_DIR, device=device)


    # Create the callback: check every 1000 steps
    # callback = rlagent.utils.SaveOnBestTrainingRewardCallback(check_freq=check_freq, log_dir=tmp_dir)

    ###############################################################################
    # Train the agent

    start_train_time = time.time()
    model.learn(total_timesteps=train_timesteps)
    end_train_time = time.time()

    ###############################################################################
    # Save the trained agent

    trained_model_file_path = os.path.join(MODELS_DIR, f"{pid}_{d_str}.json")
    model.save(trained_model_file_path)


    ###############################################################################
    # Assess the agent

    eval_reward = AccentaEnv.eval(model)


    # df = pd.DataFrame(callback.rewards_list, columns=["num_timesteps", "mean_reward", "best_mean_reward"])

    # TODO: RETOURNER UN JSON AVEC :
    # - le temps d'execution
    # - l'identifiant de la machine sur laquelle l'entrainement a été fait
    # - la date et l'heure de l'entrainement
    # - le model (ou le path du fichier model sauvegardé)
    # - la liste exhaustive des paramètres de l'environnement
    # - la liste exhaustive des paramètres de l'agent
    # - la courbe de convergence ou le path du fichier de log utilisé par tensorboard
    # - évaluation finale

    model_dict = {
        "training_duration_sec": end_train_time - start_train_time,
        "training_hardware_id": socket.getfqdn(),
        "training_datetime": d_str,
        "training_pid": pid,
        "training_timesteps": train_timesteps,
        "trained_model_file_path": trained_model_file_path,
        "agent": {
            "model_name": model.__class__.__name__,
            "model_policy": policy_kwargs,
            "model_policy_class": ppo_policy_class       # TODO: non générique
        },
        "environment": env.params(),
        "logs": model.logger.dir,     # https://pytorch.org/docs/stable/tensorboard.html
        # "training_mean_reward": {},
        "eval_reward": eval_reward
    }

    with open(f"{pid}_{d_str}.json", "w") as fd:
        json.dump(model_dict, fd, sort_keys=True, indent=4, default=str)  # pretty print format

    env.close()

    return json.dumps(model_dict, sort_keys=True, indent=4, default=str)
